<?php

$config = array(

	// Storage directory 
	"data_dir"			=>	"/media/music",

	/*
	 Whether the html lyrics of songs will be stored so they don't have to get looked up each time.
	 Setting this to true will consume extra storage space but will save on bandwidth since ajax requests for lyric crawling
	 go through the server
	*/
	"store_lyrics"		=> true,

	"youtube"			=> array(

		"enabled"			=> true,

		// Set if your ffmpeg binary is not on the system path
		// ffmpeg is required for getting songs off youtube
		"ffmpeg_location"	=> "",

		// Quality of mp3 conversion
		// Number between 0 (better) and 9 (worse)
		"ffmpeg_quality"	=> 0
	),

	// MySQL
	"mysql_host" 		=> "localhost",
	"mysql_user" 		=> "root",
	"mysql_pass"		=> "root",
	"mysql_db"			=> "ownsound",
	"mysql_port"		=> 3306,

	// Last.fm integration
	"lastfm_api_key"	=> ""





);

?>