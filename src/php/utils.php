<?php

function connect()
{
	global $config;
	$conn = mysqli_connect($config['mysql_host'], $config['mysql_user'], $config['mysql_pass'], $config['mysql_db'], $config['mysql_port'])
		or die(mysqli_error($conn));
	mysqli_set_charset($conn,"utf8");
	return $conn;
}

function getPasswordHash($password, $salt)
{
	$soup = '';
	for($i = 0; $i < strlen($password); $i++)
	{
		$soup .= $salt[$i] . $password[$i];
	}
	$soup .= substr($salt, $i, strlen($salt) - $i - 1);
	$hash = hash('sha256', $soup);

	return $hash;
}

function generateSalt($length = 64) 
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getGravatarHash()
{
	if (!isset($_SESSION['email']) || trim($_SESSION['email'])==='')
	{
		return md5($_SESSION['username']);
	}
	else
	{
		return md5($_SESSION['email']);
	}
}



function isEmpty($var)
{
	return (!isset($var) || trim($var)==='');
}

?>