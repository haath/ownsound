<?php

class LastFm
{
	private static function apiKey()
	{
		global $config;
		return $config['lastfm_api_key'];
	}

	public static function getTrack($title, $artist)
	{
		$apiKey = LastFm::apiKey();
	
		$title = urlencode($title);
		$artist = urlencode($artist);
		$url = "https://ws.audioscrobbler.com/2.0/?method=track.search&artist={$artist}&track={$title}&api_key={$apiKey}&format=json&limit=1";
		$results = json_decode(file_get_contents($url))->results->trackmatches->track;
	
		return count($results) > 0 ? $results[0] : false;
	}
	
	public static function getArtist($artistName)
	{
		$apiKey = LastFm::apiKey();
	
		$artistName = urlencode($artistName);
		$url = "https://ws.audioscrobbler.com/2.0/?method=artist.search&artist={$artistName}&api_key={$apiKey}&format=json&limit=1";
		$resp = json_decode(file_get_contents($url));
		$results =  $resp->results->artistmatches->artist;
	
		return count($results) > 0 ? $results[0] : false;
	}

	public static function getArtistInfo($artistId)
	{
		$apiKey = LastFm::apiKey();
	
		$url = "http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&mbid={$artistId}&api_key={$apiKey}&format=json&limit=1";
		$results = json_decode(file_get_contents($url));
	
		return $results->artist;
	}
	
	public static function getAlbum($albumTitle, $artistName)
	{
		$apiKey = LastFm::apiKey();
	
		$albumTitle = urlencode($albumTitle . ' ' . $artistName);
		$url = "https://ws.audioscrobbler.com/2.0/?method=album.search&album={$albumTitle}&api_key={$apiKey}&format=json&limit=1";
		$results = json_decode(file_get_contents($url))->results->albummatches->album;
	
		return count($results) > 0 ? $results[0] : false;
	}

	public static function getTrackInfo($trackId)
	{
		$apiKey = LastFm::apiKey();

		$url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key={$apiKey}&mbid={$trackId}&format=json&limit=1";
		$results = json_decode(file_get_contents($url));

		return $results->track;
	}

	public static function searchTrackInfo($title, $artist)
	{
		$apiKey = LastFm::apiKey();

		$title = urlencode($title);
		$artist = urlencode($artist);
		$url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key={$apiKey}&artist={$artist}&track={$title}&format=json&limit=1";
		$results = json_decode(file_get_contents($url));

		return $results->track;
	}
}


?>