<?php

function userToObject($user)
{
	$song = getSong($user['listening']);
	$online = $user['last_seen'] != null && (int)($user['last_seen']) < 10;

	$obj = array(
		"user_id"		=> $user['user_id'],
		"username"		=> $user['username'],
		"email"			=> isset($user['email']) ? $user['email'] : "",
		"online"		=> $online,
		"gravatar_hash"	=> $user['email'] != null ? md5($user['email']) : md5($user['username'])
	);

	if ($online && $song != null)
	{
		$obj['listening'] = $song;
	}
	else
	{
		$last_seen = isset($user['last_seen']) ? $user['last_seen'] : -1;
		$obj['last_seen'] = (int)($last_seen);
	}

	return $obj;
}

function getSong($songId)
{
	$conn = connect();
	$sql = "SELECT mbid, title, album_id, duration
			FROM tracks
			WHERE mbid='{$songId}'
			LIMIT 1";
	$query = mysqli_query($conn, $sql) or die(mysqli_error($conn));

	$song = mysqli_fetch_assoc($query);
	return $song ? songToObject($song) : null;
}

function songToObject($song)
{
	$album = getAlbum($song['album_id']);
	$artist = getArtist($album['artist_id']);

	return array(
		"mbid"		=> $song['mbid'],
		"title"		=> $song['title'],
		"duration"	=> (int)$song['duration'],
		"album"		=> array(
			"mbid"		=> $album['mbid'],
			"name"		=> $album['name'],
			"img"		=> $album['img'],
			"year"		=> (int)$album['year']
		),
		"artist"	=> array(
			"mbid"		=> $artist['mbid'],
			"name"		=> $artist['name'],
			"img"		=> $artist['img']
		)
	);
}

function getAlbum($albumId)
{
	$conn = connect();
	$sql = "SELECT mbid, artist_id, album_name, year, img_small, img_medium, img_large
			FROM albums
			WHERE mbid='{$albumId}'
			LIMIT 1";
	$query = mysqli_query($conn, $sql) or die(mysqli_error($conn));

	$album = mysqli_fetch_assoc($query);
	return $album ? albumToObject($album) : null;
}

function albumToObject($album)
{
	$obj = array(
		"mbid"		=> $album['mbid'],
		"artist_id"	=> $album['artist_id'],
		"name"		=> $album['album_name'],
		"year"		=> $album['year'],
		"img"		=> array(
			"small"		=> $album['img_small'],
			"medium"	=> $album['img_medium'],
			"large"		=> $album['img_large']
		)
	);

	if (array_key_exists('song_count', $album))
		$obj['song_count'] = (int)$album['song_count'];

	return $obj;
}

function getArtist($artistId)
{
	$conn = connect();
	$sql = "SELECT mbid, artist_name, img_small, img_medium, img_large
			FROM artists
			WHERE mbid='{$artistId}'
			LIMIT 1";
	$query = mysqli_query($conn, $sql) or die(mysqli_error($conn));

	$artist = mysqli_fetch_assoc($query);
	return $artist ? artistToObject($artist) : null;
}

function artistToObject($artist)
{
	return array(
		"mbid"		=> $artist['mbid'],
		"name"		=> $artist['artist_name'],
		"img"		=> array(
			"small"		=> $artist['img_small'],
			"medium"	=> $artist['img_medium'],
			"large"		=> $artist['img_large']
		)
	);
}



/*
	Storing Last.fm objects in the database
*/
function storeArtist($artist)
{
	$conn = connect();

	$mbid = mysqli_real_escape_string($conn, $artist->mbid);
	$name = mysqli_real_escape_string($conn, trim($artist->name));
	$img_small = mysqli_real_escape_string($conn, getImages($artist)['small']);
	$img_medium = mysqli_real_escape_string($conn, getImages($artist)['medium']);
	$img_large = mysqli_real_escape_string($conn, getImages($artist)['large']);

	$sql = "INSERT IGNORE INTO artists (mbid, artist_name, img_small, img_medium, img_large, date_added)
								VALUES ('{$mbid}', '{$name}', '{$img_small}', '{$img_medium}', '{$img_large}', NOW())";

	mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

function storeAlbum($album, $artistId, $year)
{
	$conn = connect();

	$mbid = mysqli_real_escape_string($conn, $album->mbid);
	$name = mysqli_real_escape_string($conn, trim($album->name));
	$img_small = mysqli_real_escape_string($conn, getImages($album)['small']);
	$img_medium = mysqli_real_escape_string($conn, getImages($album)['medium']);
	$img_large = mysqli_real_escape_string($conn, getImages($album)['large']);

	$artistId = mysqli_real_escape_string($conn, $artistId);
	$year = mysqli_real_escape_string($conn, $year);

	if ((!isset($mbid) || trim($mbid)===''))
		$mbid = md5($name . $year);

	$sql = "INSERT IGNORE INTO albums (mbid, artist_id, album_name, year, img_small, img_medium, img_large, date_added)
								VALUES ('{$mbid}', '{$artistId}', '{$name}', '{$year}', '{$img_small}', '{$img_medium}', '{$img_large}', NOW())";

	mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

function storeTrack($track, $duration, $albumId, $size, $uploader)
{
	$conn = connect();

	$mbid = mysqli_real_escape_string($conn, $track->mbid);
	$title = mysqli_real_escape_string($conn, trim($track->name));
	$duration = mysqli_real_escape_string($conn, $duration);
	$albumId = mysqli_real_escape_string($conn, $albumId);
	$uploader = mysqli_real_escape_string($conn, $uploader);

	if ((!isset($mbid) || trim($mbid)===''))
		$mbid = md5($title . $albumId);

	$sql = "INSERT IGNORE INTO tracks (mbid, album_id, title, duration, size, uploader, date_added)
								VALUES ('{$mbid}', '{$albumId}', '{$title}', '{$duration}', '{$size}', '{$uploader}', NOW())";

	mysqli_query($conn, $sql) or die(mysqli_error($conn));
}


function getImages($obj)
{
	if (!isset($obj->image))
	{
		return array(
			"small"	=> "",
			"medium" => "",
			"large" => ""
		);
	}

	$imgArray = $obj->image;
	
	$images = array();

	foreach ($imgArray as $image)
	{
		$images[$image->size] = $image->{'#text'};
	}

	$small = isset($images['medium']) ? $images['medium'] : $images['small'];
	$medium = isset($images['large']) ? $images['large'] : $images['medium'];
	$large = isset($images['extralarge']) ? $images['large'] : $images['large'];

	return array(
		"small" 	=> $small,
		"medium"	=> $medium,
		"large"		=> $large
	);
}

?>