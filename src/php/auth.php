<?php

$page = isset($page) ? $page : "api";

session_start();

$auth = auth();

if ($page == "signout")
{
	// If the user was logged in through a token, reset it
	if (isset($_SESSION['token_series']))
	{
		$conn = connect();
		$series = mysqli_real_escape_string($conn, $_SESSION['token_series']);
		$uid = mysqli_real_escape_string($conn, $_SESSION['user_id']);

		mysqli_query($conn,
			"DELETE FROM login_tokens WHERE user_id='{$uid}' AND series='{$series}'"
		) or die(mysqli_error($conn));
	}

	session_destroy();
	header("Location: /", true, 301);
	exit();
}

session_write_close();


if (!$auth && $page != "login")
{
	header("Location: login", true, 301);
	exit();
}
if ($auth && $page == "login")
{
	header("Location: /", true, 301);
	exit();
}


function auth()
{
	if (sessionLoggedIn())
	{
		return true;
	}
	if (postLogIn() || cookieLogIn())
	{
		return true;
	}
	return false;
}

function sessionLoggedIn()
{
	return isset($_SESSION['user_id']);
}

function postLogIn()
{
	if (!isset($_POST['username']) || !isset($_POST['password']))
	{
		return false;
	}

	$conn = connect();

	$username = mysqli_real_escape_string($conn, $_POST['username']);
	$password = mysqli_real_escape_string($conn, $_POST['password']);

	$query = mysqli_query($conn,
		"SELECT user_id, username, password, salt, email, admin, quota 
		FROM users WHERE LOWER(username) = LOWER('{$username}')"
	) or die(mysqli_error($conn));
	
	if($res = mysqli_fetch_assoc($query)) {

		$salt = $res['salt'];

		$hash = getPasswordHash($password, $salt);

		if($hash == $res['password'])
		{
			// Check if we should send a "Remember Me" cookie
			if (isset($_POST['remember_me']))
			{
				setLoginToken($res['user_id']);
			}

			// Successful login
			$_SESSION['user_id'] 	= $res['user_id'];
			$_SESSION['username'] 	= $res['username'];
			$_SESSION['email'] 		= $res['email'];
			$_SESSION['admin']		= $res['admin'] == 0x01 ? true : false;
			$_SESSION['quota'] 		= $res['quota'];

			return true;
		}
	}

	?>
	<div class="alert alert-danger">
		Invalid Username and/or Password.
	</div>
	<?php

	return false;
} 

function cookieLogIn()
{
	if (isset($_COOKIE['auth']))
	{
		$p = explode(':', $_COOKIE['auth']);
		
		if (count($p) == 2)
		{
			$conn = connect();
			$series = mysqli_real_escape_string($conn, $p[0]);
			$token = mysqli_real_escape_string($conn, $p[1]);

			$sql = "SELECT user_id, hash, `timestamp`
					FROM login_tokens
					WHERE series='{$series}'";
			$query = mysqli_query($conn, $sql) or die(mysqli_error($conn));

			$res = mysqli_fetch_assoc($query);

			if ($res)
			{
				$sentHash = hash('sha256', $token);

				if ($res['hash'] == $sentHash)
				{
					$uid = mysqli_real_escape_string($conn, $res['user_id']);

					// Successful login
					$sql = "SELECT user_id, username, email, admin, quota 
							FROM users
							WHERE user_id='{$uid}'";
					$query = mysqli_query($conn, $sql) or die(mysqli_error($conn));

					$res = mysqli_fetch_assoc($query);
					$_SESSION['user_id'] 		= $res['user_id'];
					$_SESSION['username'] 		= $res['username'];
					$_SESSION['email'] 			= $res['email'];
					$_SESSION['admin']			= $res['admin'] == 0x01 ? true : false;
					$_SESSION['quota'] 			= $res['quota'];
					$_SESSION['token_series']	= $series;

					refreshLoginToken($res['user_id'], $series);

					return true;
				}
			}
		}
	}
	return false;
}

function refreshLoginToken($user_id, $series)
{
	$conn = connect();
	
	$token = generateSalt();
	$hash = hash('sha256', $token);
	
	$sql = "UPDATE login_tokens
			SET hash='{$hash}', `timestamp`=NOW()
			WHERE user_id='{$user_id}' AND series='{$series}'";
	mysqli_query($conn, $sql) or die(mysqli_error($conn));

	$cookieVal = $series . ':' . $token;
	
	setcookie('auth', $cookieVal, time() + (86400 * 15), "/");
}

function setLoginToken($user_id)
{
	$conn = connect();
	
	$token = generateSalt();
	$hash = hash('sha256', $token);

	// Insert hash in DB
	$sql = "INSERT INTO login_tokens (user_id, hash, `timestamp`)
					VALUES ('{$user_id}', '{$hash}', NOW())";
	mysqli_query($conn, $sql) or die(mysqli_error($conn));

	// Set cookie
	$series = mysqli_insert_id($conn);
	$cookieVal = $series . ':' . $token;

	setcookie('auth', $cookieVal, time() + (86400 * 15), "/");
}

?>