<?php

require("../php/config.php");
require("../php/utils.php");
require("../php/auth.php");

require("../php/api_functions.php");

header("Content-Type: application/json; charset=utf-8");

$conn = connect();

$sql = "SELECT playlist_id, name 
		FROM playlists";

if (isset($_GET['playlist']))
{
	$id = mysqli_real_escape_string($conn, $_GET['playlist']);
	$sql .= " WHERE playlist_id='{$id}'";
}

$query = mysqli_query($conn, $sql) or die(mysqli_error($conn));

$resp = array();

while ($row = mysqli_fetch_assoc($query))
{
	$playlist = array(
		"playlist_id"	=> $row['playlist_id'],
		"name"			=> $row['name'],
		"songs"			=> array()
	);

	$sql = "SELECT song_mbid FROM playlist_songs WHERE playlist_id='{$playlist_id}'";
	$q = mysqli_query($conn, $sql) or die(mysqli_error($conn));

	while ($s = mysqli_fetch_assoc($q))
	{
		$playlist['songs'][] = $s['song_mbid'];
	}

	$resp[] = $playlist;
}

echo json_encode($resp);

?>