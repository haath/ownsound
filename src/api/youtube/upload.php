<?php


require("../../php/config.php");
require("../../php/utils.php");
require("../../php/auth.php");
require("../../php/api_functions.php");

require("../../php/lib/lastfm.php");

header("Content-Type: application/json; charset=utf-8");


$conn = connect();

$title = mysqli_real_escape_string($conn, trim($_POST['title']));
$artist = mysqli_real_escape_string($conn, trim($_POST['artist']));
$url = mysqli_real_escape_string($conn, trim($_POST['url']));

// Get song details
$info = getSongInfo($title, $artist);

// Download from YouTube
$fileDest = $config['data_dir'] . '/audio/' . $info->track->mbid . '.webm';
$cmd = "youtube-dl -f webm --restrict-filenames --print-json -o \"{$fileDest}\" {$url} 2>&1";
$cmd_output = shell_exec($cmd);

$youtube_info = json_decode($cmd_output);

// Store in database
storeArtist($info->artist);
storeAlbum($info->album, $info->artist->mbid, ' ');

// Start the conversion task
$convert_output = startConvert($info->track, $info->album->mbid, $youtube_info->duration);

// Echo result
$resp = array(
	"url"				=> $url,
	"title"				=> $title,
	"artist"			=> $artist,
	"fileDest"			=> $fileDest,
	"cmd"				=> $cmd,
	"convert_output"	=> $convert_output,
	"youtube_info"			=> $youtube_info,
	"info"				=> $info
);

echo json_encode($resp);

function getSongInfo($title, $artistName)
{
	$track = LastFm::searchTrackInfo($title, $artistName);

	if (!isset($track->mbid) || isEmpty($track->mbid))
	{
		$track->mbid = md5($track->name);
	}

	$artist = LastFm::getArtistInfo($track->artist->mbid);
	
	return (object)array(
		"track"		=> (object)array(
			"name"		=> $track->name,
			"mbid"		=> $track->mbid,
			"duration"	=> (int)$track->duration / 1000
		),
		"artist"	=> (object)array(
			"name"		=> $artist->name,
			"mbid"		=> $artist->mbid,
			"image"		=> $artist->image
		),
		"album"		=> (object)array(
			"name"		=> $track->album->title,
			"mbid"		=> $track->album->mbid,
			"image"		=> $track->album->image
		)
	);
}

function startConvert($track, $albumId, $duration)
{
	$prefix = $_SERVER['HTTPS'] ? 'https://' : 'http://';
	$url = $prefix . $_SERVER['HTTP_HOST'] . "/api/youtube/convert.php";
	$postData = "&mbid=" . urlencode($track->mbid);
	$postData .= "&name=" . urlencode($track->name);
	$postData .= "&duration=" . urlencode($duration);
	$postData .= "&album_id=" . urlencode($albumId);
	$postData .= "&user_id=" . urlencode($_SESSION['user_id']);
	$postData .= "&secret=penis";

	$curlH = curl_init();
	curl_setopt($curlH, CURLOPT_URL, $url);
	curl_setopt($curlH, CURLOPT_POST, TRUE);
	curl_setopt($curlH, CURLOPT_POSTFIELDS, $postData);
	curl_setopt($curlH, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curlH, CURLOPT_TIMEOUT, 5);
	$result = curl_exec($curlH);

	$resp = array(
		"url"		=> $url,
		"post"		=> $postData,
		"error"		=> curl_error($curlH),
		"errno"		=> curl_errno($curlH)
	);

	$resp['response'] = json_decode($result);
	
	if (json_last_error() != JSON_ERROR_NONE)
		$resp['response'] = $result;

	return $resp;
}
?>