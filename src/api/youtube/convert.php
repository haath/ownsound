<?php

require("../../php/config.php");
require("../../php/utils.php");

require("../../php/api_functions.php");

ob_end_clean();
header("Connection: close");
ignore_user_abort(true);

header('Content-Type: application/json');

ob_start();

$resp = array();

if (!isset($_POST['mbid']) || !isset($_POST['secret']) || $_POST['secret'] != 'penis'
	|| !isset($_POST['name']) || !isset($_POST['duration']) || !isset($_POST['album_id'])
	|| !isset($_POST['user_id']))
{
	$resp['msg'] = "Missing parameters.";
	echo json_encode($resp);
	die();
}


$inFile = $config['data_dir'] . '/audio/' . $_POST['mbid'] . '.webm';
$outFile = $config['data_dir'] . '/audio/' . $_POST['mbid'] . '.mp3';
$progressFile = $config['data_dir'] . '/audio/' . $_POST['mbid'] . '.progress';

$resp = array(
	"infile"		=> $inFile,
	"outFile"		=> $outFile,
	"progressFile"	=> $progressFile
);


if (!file_exists($inFile))
{
	$resp['msg'] = "File not found.";
	echo json_encode($resp);
	die();
}

if (file_exists($outFile))
{
	$resp['msg'] = "Target file already exists.";
	echo json_encode($resp);
	die();
}


// Begin conversion

$ffmpeg = isEmpty($config['youtube']['ffmpeg_location']) ? "ffmpeg" : $config['youtube']['ffmpeg_location'];
$ffmpeg_quality = $config['youtube']['ffmpeg_quality'];

$cmd = "{$ffmpeg} -y -i file:'{$inFile}' -vn -acodec libmp3lame -q:a {$ffmpeg_quality} file:'{$outFile}' 1> '{$progressFile}' 2>&1";

$resp['cmd'] = $cmd;
echo json_encode($resp);

$size = ob_get_length();
header("Content-Length: $size");
ob_end_flush();
flush();

$cmd_output = shell_exec($cmd);

// Clean up...
unlink($inFile);
unlink($progressFile);



// Get mp3 file size
$size = filesize($outFile);

// Submit song to DB
$conn = connect();

$track = (object)array(
	"mbid"		=> mysqli_real_escape_string($conn, $_POST['mbid']),
	"name"		=> mysqli_real_escape_string($conn, $_POST['name'])
);
$duration = mysqli_real_escape_string($conn, $_POST['duration']);
$album_id = mysqli_real_escape_string($conn, $_POST['album_id']);
$user_id = mysqli_real_escape_string($conn, $_POST['user_id']);


storeTrack($track, $duration, $album_id, $size, $user_id);

?>