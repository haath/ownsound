<?php

require("../../php/config.php");
require("../../php/utils.php");
require("../../php/auth.php");
require("../../php/api_functions.php");

header("Content-Type: application/json; charset=utf-8");

if (!isset($_GET['mbid']))
{
	die();
}

$resp = array();

$progressFile = $config['data_dir'] . '/audio/' . $_GET['mbid'] . '.progress';
$targetFile = $config['data_dir'] . '/audio/' . $_GET['mbid'] . '.mp3';

$content = @file_get_contents($progressFile);
if($content)
{
	//get duration of source
	preg_match("/Duration: (.*?), start:/", $content, $matches);

	$rawDuration = $matches[1];

	//rawDuration is in 00:00:00.00 format. This converts it to seconds.
	$ar = array_reverse(explode(":", $rawDuration));
	$duration = floatval($ar[0]);
	if (!empty($ar[1])) $duration += intval($ar[1]) * 60;
	if (!empty($ar[2])) $duration += intval($ar[2]) * 60 * 60;

	//get the time in the file that is already encoded
	preg_match_all("/time=(.*?) bitrate/", $content, $matches);

	$rawTime = array_pop($matches);

	//this is needed if there is more than one match
	if (is_array($rawTime)){$rawTime = array_pop($rawTime);}

	//rawTime is in 00:00:00.00 format. This converts it to seconds.
	$ar = array_reverse(explode(":", $rawTime));
	$time = floatval($ar[0]);
	if (!empty($ar[1])) $time += intval($ar[1]) * 60;
	if (!empty($ar[2])) $time += intval($ar[2]) * 60 * 60;

	//calculate the progress
	$progress = round(($time/$duration), 2);

	$resp['status']		= "converting";
	$resp['duration'] 	= $duration;
	$resp['cur_time'] 	= $time;
	$resp['progress'] 	= $progress;
}
else if (file_exists($targetFile))
{
	$resp['status'] = "done";
}
else
{
	$resp['status'] = "not_found";
}

echo json_encode($resp);

?>