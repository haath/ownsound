<?php

require("../php/config.php");
require("../php/utils.php");
require("../php/auth.php");
require("../php/api_functions.php");

require("../php/lib/lastfm.php");

$conn = connect();
print_r($_POST);
exit();
/*
	Check if the song is being deleted	
*/
if (isset($_POST['song-mbid']) && isset($_POST['delete']) && $_POST['delete']=='1')
{
	$song_mbid = mysqli_real_escape_string($conn, $_POST['song-mbid']);

	// Delete from DB
	$sql = "DELETE FROM tracks WHERE mbid='{$song_mbid}'";
	mysqli_query($conn, $sql) or die(mysqli_error($conn));

	// Delete from storage
	$file = $config['data_dir'] . '/audio/' . $song_mbid . '.mp3';
	unlink($file);

	echo "Deleted Song: " . $song_mbid;
	exit();
}


$queries = array();

/*
	Update artist
*/
if (isset($_POST['artist-mbid']) && isset($_POST['artist']))
{
	$artist_mbid = mysqli_real_escape_string($conn, $_POST['artist-mbid']);
	$artist = mysqli_real_escape_string($conn, $_POST['artist']);

	$lastfmArtist = LastFm::getArtist($_POST['artist']);

	
	if ($lastfmArtist->mbid == $artist_mbid)
	{
		$queries[] = "UPDATE artists 
				SET artist_name='{$artist}'
				WHERE mbid='{$artist_mbid}'";
	}
	else
	{
		storeArtist($lastfmArtist);
		$queries[] = "DELETE FROM artists WHERE mbid='{$artist_mbid}'";
		$queries[] = "UPDATE albums SET artist_id='{$lastfmArtist->mbid}' WHERE artist_id='{$artist_mbid}'";
	}
}

/*
	Update album
*/
if (isset($_POST['album-mbid']) && isset($_POST['album']) && isset($_POST['year']))
{
	$album_mbid = mysqli_real_escape_string($conn, $_POST['album-mbid']);
	$album = mysqli_real_escape_string($conn, $_POST['album']);
	$year = mysqli_real_escape_string($conn, $_POST['year']);

	$lastfmAlbum = LastFm::getAlbum($_POST['album'], $_POST['artist']);
	
	if ($lastfmAlbum->mbid == $album_mbid)
	{
		$queries[] = "UPDATE albums 
				SET album_name='{$album}', year='{$year}'
				WHERE mbid='{$album_mbid}'";
	}
	else
	{
		print_r($lastfmAlbum);
		storeAlbum($lastfmAlbum, $artistId, $year);
		$queries[] = "DELETE FROM albums WHERE mbid='{$album_mbid}'";
		$queries[] = "UPDATE tracks SET album_id='{$lastfmAlbum->mbid}' WHERE album_id='{$album_mbid}'";
	}
}



/*
	Update song
*/
if (isset($_POST['song-mbid']) && isset($_POST['title']) && isset($_POST['duration']))
{
	$song_mbid = mysqli_real_escape_string($conn, $_POST['song-mbid']);
	$title = mysqli_real_escape_string($conn, $_POST['title']);
	$duration = mysqli_real_escape_string($conn, $_POST['duration']);

	$queries[] = "UPDATE tracks 
				SET title='{$title}', duration='{$duration}'
				WHERE mbid='{$song_mbid}'";
}

foreach ($queries as $query)
{
	echo $query . "\t\n";
	mysqli_query($conn, $query) or die(mysqli_error($conn));
}
?>