<?php

require("../php/config.php");
require("../php/utils.php");
require("../php/auth.php");

require("../php/api_functions.php");

if (!isset($_POST['user_id']))
{
	exit();
}

$conn = connect();

$id = mysqli_real_escape_string($conn, $_POST['user_id']);

if ($_SESSION['user_id'] != $id && !$_SESSION['admin'])
{
	// unauthorized
	exit();
}

$sql = "UPDATE users SET user_id=user_id";

if (isset($_POST['username']))
{
	$username = mysqli_real_escape_string($conn, $_POST['username']);
	$sql .= ", username='{$username}'";
}

if (isset($_POST['email']))
{
	$email = mysqli_real_escape_string($conn, $_POST['email']);
	$sql .= ", email='{$email}'";
}

if (isset($_POST['password']) && isset($_POST['password2']))
{
	$p1 = mysqli_real_escape_string($conn, $_POST['password']);
	$p2 = mysqli_real_escape_string($conn, $_POST['password2']);

	if ($p1 != "" || $p2 != "")
	{
		if ($p1 != $p2)
		{
			echo "Passwords do not match.";
			exit();
		}
	
		$salt = generateSalt();
		$hash = getPasswordHash($p1, $salt);
	
		$sql .= ", password='{$hash}', salt='{$salt}'";
	}
}

$sql .= " WHERE user_id='{$id}'";

mysqli_query($conn, $sql) or die(mysqli_error($conn));

echo "OK";

?>