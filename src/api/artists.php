<?php

require("../php/config.php");
require("../php/utils.php");
require("../php/auth.php");

require("../php/api_functions.php");

header("Content-Type: application/json; charset=utf-8");

$conn = connect();

$sql = "SELECT mbid, artist_name, img_small, img_medium, img_large
		FROM artists";

if (isset($_GET['artist']))
{
	$mbid = mysqli_real_escape_string($conn, $_GET['artist']);

	$sql .= " WHERE mbid='{$mbid}'";
}

$sql .= " ORDER BY artist_name";

$query = mysqli_query($conn, $sql) or die(mysqli_error($conn));

$resp = array();

while ($row = mysqli_fetch_assoc($query))
{
	$artist = artistToObject($row);

	if (!isset($_GET['list']))
	{
		$artist['albums'] = array();
		$mbid = mysqli_real_escape_string($conn, $artist['mbid']);
	
		$sql = "SELECT A.mbid, A.artist_id, A.album_name, A.year, A.img_small, A.img_medium, A.img_large,
					COUNT(T.mbid) as song_count
				FROM albums A, tracks T 
				WHERE A.artist_id='{$mbid}' AND A.mbid=T.album_id
				GROUP BY A.mbid
				ORDER BY A.year DESC";
		$albumQuery = mysqli_query($conn, $sql) or die(mysqli_error($conn));
	
		while ($album = mysqli_fetch_assoc($albumQuery))
		{
			$artist['albums'][] = albumToObject($album);
		}
	}

	$resp[] = $artist;
}

echo json_encode($resp);

?>