<?php

require("../php/config.php");
require("../php/utils.php");
require("../php/auth.php");

require("../php/api_functions.php");


if (!$_SESSION['admin'])
{
	// Unauthorized
	exit();
}

if (!isset($_POST['username']) || !isset($_POST['password']) || !isset($_POST['password2']))
{
	echo "Incomplete registration form";
	exit();
}

$conn = connect();

$username = mysqli_real_escape_string($conn, $_POST['username']);
$p1 = mysqli_real_escape_string($conn, $_POST['password']);
$p2 = mysqli_real_escape_string($conn, $_POST['password2']);
$email = mysqli_real_escape_string($conn, isset($_POST['email']) ? $_POST['email'] : "");
$quota = 1000000 * mysqli_real_escape_string($conn, isset($_POST['quota']) ? $_POST['quota'] : "");

if ($p1 == "")
{
	echo "Password cannot be empty.";
	exit();
}

if ($p1 != $p2)
{
	echo "Passwords do not match.";
	exit();
}

$salt = generateSalt();
$hash = getPasswordHash($p1, $salt);

$sql = "INSERT INTO users (username, password, salt, email, admin, quota) 
		VALUES ('{$username}', '{$hash}', '{$salt}', '{$email}', 0, '{$quota}')";

mysqli_query($conn, $sql) or die(mysqli_error($conn));

echo "OK";

?>