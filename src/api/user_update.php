<?php

define("SONG_LISTEN_THRESHOLD", 60);


require("../php/config.php");
require("../php/utils.php");
require("../php/auth.php");

$conn = connect();

$id = mysqli_real_escape_string($conn, $_SESSION['user_id']);
$ip = mysqli_real_escape_string($conn, $_SERVER['REMOTE_ADDR']);

if (isset($_POST['song']))
{
	$song = mysqli_real_escape_string($conn, $_POST['song']);
	$sql = "UPDATE users SET last_seen=NOW(), ip='{$ip}', listening='{$song}' WHERE user_id='{$id}'";
}
else
{
	$sql = "UPDATE users SET last_seen=NOW(), ip='{$ip}', listening=NULL WHERE user_id='{$id}'";
}

mysqli_query($conn, $sql) or die(mysqli_error($conn));

/*
	Check if we should update the user listening history
*/
if (isset($_POST['song']))
{
	$song = mysqli_real_escape_string($conn, $_POST['song']);

	session_start();
	if (!isset($_SESSION['listening']) || $_SESSION['listening']['song'] != $song)
	{
		$_SESSION['listening'] = array(
			'song'		=> $song,	
			'started'	=> time(),
			'stored'	=> false
		);
	}
	else if ($_SESSION['listening']['song'] == $song && !$_SESSION['listening']['stored'])
	{
		if (time() - $_SESSION['listening']['started'] > SONG_LISTEN_THRESHOLD)
		{
			$sql = "INSERT INTO user_listens (user_id, track_id, `timestamp`)
					VALUES ('{$id}', '{$song}', NOW())";

			mysqli_query($conn, $sql) or die(mysqli_error($conn));

			$_SESSION['listening']['stored'] = true;
		}
	}
	session_write_close();
}

?>