<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

if (!isset($_FILES['songs']))
{
	exit();
}

require("../php/config.php");
require("../php/utils.php");
require("../php/auth.php");
require("../php/api_functions.php");

require("../php/lib/lastfm.php");
require("../php/lib/mp3.php");
require("../php/lib/mp3_supl.php");


header("Content-Type: application/json");

$files = $_FILES['songs'];

$resp = array();

$resp['files'] = $files;

$total_size = 0;
for ($i = 0; $i < count($files['size']); $i++)
{
	$total_size += $files['size'][$i];
}

$resp['size'] = $total_size;

/*
	First we check if the user can upload without exceeding their quota.
*/
$conn = connect();
$user_id = mysqli_real_escape_string($conn, $_SESSION['user_id']);
$sql = "SELECT SUM(size) FROM tracks WHERE uploader='{$user_id}'";
$query = mysqli_query($conn, $sql);

$user_total_uploads = mysqli_fetch_row($query)[0];

if ($_SESSION['quota'] > 0 && $user_total_uploads + $total_size > $_SESSION['quota'])
{
	echo "This upload exceeds your quota.";
	exit();
}


$resp['user_total_uploads'] = $user_total_uploads;
$resp['songs'] = array();

for ($i = 0; $i < count($files['name']); $i++)
{
	$name = $files['name'][$i];
	$file = $files['tmp_name'][$i];
	$size = $files['size'][$i];

	$resp[] = uploadFile($name, $file, $size);
}


echo json_encode($resp);





function uploadFile($name, $file, $size)
{
	global $config;

	$resp = array();
	
	$mp3 = new Mp3File();
	$mp3->load($name, $file);

	//$resp['id3'] = id3_get_tag($file);

	$resp['metadata'] = array(
		"title" => $mp3->title,
		"artist" => $mp3->artist,
		"album" => $mp3->album,
		"year" => $mp3->year,
		"duration" => $mp3->getDuration(),
	);

	$track = $mp3->getTrack();
	$artist = $mp3->getArtist();
	$album = $mp3->getAlbum();

	$resp['track'] = $track;

	if ($artist)
	{
		$resp['artist'] = $artist;
		storeArtist($artist);
	}

	if ($album)
	{
		$resp['album'] = $album;
		storeAlbum($album, $artist->mbid, $mp3->year);
	}

	storeTrack($track, $mp3->getDuration(), $album->mbid, $size, $_SESSION['user_id']);

	$fileDest = $config['data_dir'] . '/audio/' . $track->mbid . '.mp3';
	$resp['file'] = $fileDest;

	$resp['fileMove'] = move_uploaded_file($file, $fileDest);

	return $resp;
}





?>