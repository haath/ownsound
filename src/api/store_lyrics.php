<?php

require("../php/config.php");
require("../php/utils.php");
require("../php/auth.php");

require("../php/api_functions.php");

if (!isset($_POST['song']) || !isset($_POST['lyrics']))
{
	die();
}

if (!$config['store_lyrics'])
{
	die();
}

$mbid = $_POST['song'];
$filePath = $config['data_dir'] . '/lyrics/' . $mbid . '.html';

file_put_contents($filePath, $_POST['lyrics']);

?>