<?php

require("../../php/config.php");
require("../../php/utils.php");
require("../../php/auth.php");

require("../../php/api_functions.php");

if (!isset($_GET['artist']) || !isset($_GET['title']))
{
	exit();
}

header('Content-Type: text/html');

$title = strtolower(str_replace(' ', '-', $_GET['title']));
$artist = str_replace(' ', '-', $_GET['artist']);

$url = "https://genius.com/{$artist}-{$title}-lyrics";

echo file_get_contents($url)

?>