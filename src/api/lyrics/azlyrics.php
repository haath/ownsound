<?php

require("../../php/config.php");
require("../../php/utils.php");
require("../../php/auth.php");

require("../../php/api_functions.php");

if (!isset($_GET['artist']) || !isset($_GET['title']))
{
	exit();
}

header('Content-Type: text/html');

$title = strtolower(str_replace(' ', '', $_GET['title']));
$artist = strtolower(str_replace(' ', '', $_GET['artist']));

$url = "https://www.azlyrics.com/lyrics/{$artist}/{$title}.html";

echo file_get_contents($url)

?>