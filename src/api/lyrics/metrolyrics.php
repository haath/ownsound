<?php

require("../../php/config.php");
require("../../php/utils.php");
require("../../php/auth.php");

require("../../php/api_functions.php");

if (!isset($_GET['artist']) || !isset($_GET['title']))
{
	exit();
}

header('Content-Type: text/html');

$title = str_replace(' ', '-', $_GET['title']);
$artist = str_replace(' ', '-', $_GET['artist']);

$url = "http://www.metrolyrics.com/{$title}-lyrics-{$artist}.html";

echo file_get_contents($url)

?>