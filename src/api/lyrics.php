<?php


require("../php/config.php");
require("../php/utils.php");
require("../php/auth.php");

require("../php/api_functions.php");

header('Content-Type: text/html');

if (!isset($_GET['song']))
{
	http_response_code(404);
	die();
}

$mbid = $_GET['song'];
$filePath = $config['data_dir'] . '/lyrics/' . $mbid . '.html';

if (!file_exists($filePath))
{
	http_response_code(404);
	die();
}

readfile($filePath);

?>