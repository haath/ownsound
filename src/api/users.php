<?php

require("../php/config.php");
require("../php/utils.php");
require("../php/auth.php");

require("../php/api_functions.php");

header("Content-Type: application/json");

$conn = connect();
$sql = "SELECT user_id, username, email, UNIX_TIMESTAMP() - UNIX_TIMESTAMP(last_seen) as last_seen, listening 
		FROM users";

if (isset($_GET['me']))
{
	$id = mysqli_real_escape_string($conn, $_SESSION['user_id']);
	$sql .= " WHERE user_id='{$id}'";
}

$sql .= " ORDER BY username";

$query = mysqli_query($conn, $sql);

$resp = array();

while ($row = mysqli_fetch_assoc($query))
{
	$resp[] = userToObject($row);
}

echo json_encode($resp);

?>