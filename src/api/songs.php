<?php

require("../php/config.php");
require("../php/utils.php");
require("../php/auth.php");

require("../php/api_functions.php");

header("Content-Type: application/json");

$conn = connect();

$sql = "SELECT T.mbid as mbid, T.album_id as album_id, title as title, duration as duration, uploader as uploader
		FROM tracks T, albums M, artists A
		WHERE T.album_id=M.mbid AND M.artist_id=A.mbid";

if (isset($_GET['song']))
{
	$mbid = mysqli_real_escape_string($conn, $_GET['song']);

	$sql .= " AND T.mbid='{$mbid}'";
}

if (isset($_GET['artist']))
{
	$mbid = mysqli_real_escape_string($conn, $_GET['artist']);

	$sql .= " AND A.mbid='{$mbid}'";
}

if (isset($_GET['album']))
{
	$mbid = mysqli_real_escape_string($conn, $_GET['album']);

	$sql .= " AND M.mbid='{$mbid}'";
}

if (isset($_GET['user']))
{
	$user_id = mysqli_real_escape_string($conn, $_GET['user']);

	$sql .= " AND T.uploader='{$user_id}'";
}
else if (!isset($_GET['song']))
{
	$user_id = mysqli_real_escape_string($conn, $_SESSION['user_id']);

	$sql .= " AND (T.uploader='{$user_id}' OR T.uploader IN (SELECT can_view FROM user_visibility WHERE user_id='{$user_id}'))";
}

if (isset($_GET['search']))
{
	$search = mysqli_real_escape_string($conn, $_GET['search']);
	$words = explode(' ', $search);
	foreach ($words as $word)
	{
		$sql .= " AND (T.title LIKE '%{$word}%' OR M.album_name LIKE '%{$word}%' OR A.artist_name LIKE '%{$word}%')";
	}
}



if (isset($_GET['sort']))
{
	$param = mysqli_real_escape_string($conn, $_GET['sort']);
	$sql .= getOrdering($param);
}

if (isset($_GET['limit']))
{
	$limit = mysqli_real_escape_string($conn, $_GET['limit']);
	$sql .= " LIMIT {$limit}";
}

if (isset($_GET['offset']))
{
	$offset = mysqli_real_escape_string($conn, $_GET['offset']);
	$sql .= " OFFSET {$offset}";
}

$query = mysqli_query($conn, $sql) or die(mysqli_error($conn));

$resp = array();

while ($row = mysqli_fetch_assoc($query))
{
	$resp[] = songToObject($row);
}

echo json_encode($resp);




function getOrdering($param)
{
	switch ($param)
	{
		case "date":
			return " ORDER BY T.date_added DESC";

		case "artist":
			return " ORDER BY A.artist_name";

		case "title":
			return " ORDER BY T.title";

		case "year":
			return " ORDER BY M.year";

		case "album":
			return " ORDER BY M.album_name";

		case "random":
			return " ORDER BY RAND()";
	}
	return "";
}


?>