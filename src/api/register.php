<?php

require("../php/config.php");
require("../php/utils.php");
require("../php/auth.php");

require("../php/api_functions.php");


if ($_SESSION['admin'] && isset($_POST['username']) && isset($_POST['password']))
{
	$conn = connect();

	$username = mysqli_real_escape_string($conn, $_POST['username']);
	$password = mysqli_real_escape_string($conn, $_POST['password']);

	$salt = generateSalt();
	$hash = getPasswordHash($password, $salt);

	$sql = "INSERT INTO users (username, password, salt) VALUES ('{$username}', '{$hash}', '{$salt}')";
	mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

?>