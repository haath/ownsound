
<!-- Main audio player		-->
<audio preload="none" id="audio-player">Your browser does not support HTML5 Audio!</audio>



<div class="container-fluid">
	<div class="row">
		<div class="col-md-4" style="padding-top: 10px; padding-bottom: 10px;">
			<div class="container-fluid app-container" id="player-container">
				<div class="row">
					<div class="col-md-4 col-xs-3">
						<img class="img-responsive" id="playing-artwork" src="img/placeholder.png">
					</div>
					<div class="col-md-8 col-xs-9" style="padding-left: 0px;">
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-10">
									<span id="playing-artist" class='clickable-text'>Artist</span>
								</div>
								<div class="col-xs-1" style="padding-top: 5px;">
									<i id="repeat-btn" title="Repeat"
										onclick="audioPlayer.toggleRepeat()" 
										class="fa fa-repeat fa-lg clickable"
										data-toggle='tooltip' data-placement='right'></i>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-xs-12">
									<h3 id="playing-song">Song</h3>
								</div>
							</div>
							<div class="row">
								&nbsp;
							</div>
							<div class="row">
								<div class="col-md-1 col-xs-1">
									<i class="fa fa-step-backward fa-2x audio-control" onclick="audioPlayer.previous()"></i>
								</div>
								<div class="col-md-1 col-xs-1">
									<i class="fa fa-step-forward fa-2x audio-control" onclick="audioPlayer.next()"></i>
								</div>
								<div class="col-md-offset-1 col-md-1 col-xs-offset-1 col-xs-1">
									<i class="fa fa-volume-up fa-lg" style="margin-top: 7px; display: block;"></i>
								</div>
								<div class="col-md-6 col-xs-6">
									<span style="margin-top: 4px; display: block;">
										<input type="text" 
										data-provide="slider"
										data-slider-min="0"
										data-slider-max="100"
										data-slider-step="1"
										data-slider-value="100"
										data-slider-handle="round"
										id="volume-slider"
										data-slider-id="volume-slider-id"
										class="audio-slider">
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 col-xs-3" style="text-align: center;">
						<i class="fa fa-play-circle fa-3x audio-control" id="audio-toggle-btn" onclick="audioPlayer.toggle()"></i>
					</div>
					<div class="col-md-10 col-xs-9">
						<span style="margin-top: 11px; display: block;">
							<div class="row">
								<input type="text" 
								data-provide="slider"
								data-slider-min="0"
								data-slider-max="10000"
								data-slider-step="1"
								data-slider-value="0"
								data-slider-handle="round"
								data-slider-rangeHighlights='[{ "start": 0, "end": 0, "class": "buffered" }]'
								id="song-slider"
								data-slider-id="song-slider-id"
								class="audio-slider">
							</div>
							<div class="row">
								<div class="col-md-2 col-xs-2" id="playback-time">
									0:00
								</div>
								<div class="col-md-offset-8 col-md-2 col-xs-offset-8 col-xs-2" id="playback-duration">
									0:00
								</div>
							</div>
						</span>
					</div>
				</div>
			</div>
			<div class="container-fluid app-container">
				<div class="row">
					<div class="col-md-12 col-xs-12">
						<ul class="nav nav-pills nav-justified">
							<li class="active"><a  data-toggle="pill" href="#artist-list">Artists</a></li>
							<li><a data-toggle="pill" href="#album-list" >Albums</a></li>
							<li><a data-toggle="pill" onclick="refreshLyrics()" href="#lyrics" >Lyrics</a></li>
						</ul>
						<br>
					</div>
				</div>
				<div class="container-fluid" >
					<div class="row tab-content" id="side-lists">
						<div id="artist-list" class="tab-pane fade in active">
							<!-- Auto-fill with Ajax -->
						</div>
						<div id="album-list" class="tab-pane fade">
							<!-- Auto-fill with Ajax -->
						</div>
						<div id="lyrics" class="tab-pane fade">
							<!-- Auto-fill with Ajax -->
						</div>
					</div>
				</div>
				<br>
			</div>
		</div>
		<div class="col-md-5 app-container">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-9 col-xs-8">
						<br>
						<form id="search-form">
							<div class="input-group input-group-lg">
								<input type="text" class="form-control" id="searchbar" name="search" placeholder="Search" required>
								<div class="input-group-btn" style="height: 100%;">
									<button class="btn btn-primary" type="submit">
										<span class="fa fa-search"></span>
									</button>
								</div>
							</div>
						</form>
					</div>
					<div class="col-md-3 col-xs-4">
						<br>
						<label class="btn btn-primary btn-block ajax-btn-container">
							<i class="fa fa-cloud-upload fa-2x" id="add-song-btn"></i>
						</label>
					</div>
				</div>
			</div>
			<div class="row filters-row">
				<div class="col-md-9 col-xs-9" id="filter-labels">
					
				</div>
				<div class="col-md-2 col-xs-2">
					<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Sort By
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a onclick='setSort("date")' href="#">Date Added</a></li>
							<li><a onclick='setSort("artist")' href="#">Artist</a></li>
							<li><a onclick='setSort("title")' href="#">Title</a></li>
							<li><a onclick='setSort("year")' href="#">Year</a></li>
							<li><a onclick='setSort("album")' href="#">Album</a></li>
							<li><a onclick='setSort("random")' href="#">Random</a></li>
						</ul>
					</div>
				</div>
			</div>
			<hr>
			<div class="container-fluid" id="song-list">
				<!-- Auto-fill with Ajax -->
			</div>
		</div>
		<div class="col-md-3" style="padding-top: 10px; padding-bottom: 10px;">
			<div class="container-fluid app-container">
				<div class="row">
					<div class="col-md-12">

					</div>
				</div>
				<div class="row">
					<div class="col-md-2 col-xs-2">
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
								<i class="fa fa-cog"></i>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a onclick='editProfile()' href="#"><i class="fa fa-user fa-lg fa-fw"></i>&nbsp;Profile</a></li>
								<?php
								if ($_SESSION['admin'])
								{
								?>
								
								<li><a onclick='createUser()' href="#"><i class="fa fa-user-plus fa-lg fa-fw"></i>&nbsp;Create User</a></li>

								<?php
								}
								?>
								<li class="divider"></li>
								<li><a href="signout"><i class="fa fa-sign-out fa-lg fa-fw"></i>&nbsp;Sign Out</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-8 col-xs-8">
							<img class="img-responsive" src="img/logo_big.png">
					</div>
				</div>
				<hr>
				<div id="user-list">
					<!-- Auto-fill with Ajax -->
				</div>
			</div>
		</div>
	</div>
</div>