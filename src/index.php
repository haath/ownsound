<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require("php/config.php");
require("php/utils.php");

$page = isset($_GET['page']) ? $_GET['page'] : "index";

require("php/auth.php");

$pages = array(
	"login" => "pages/login.html",
	"index" => "pages/index.php"
);

if (!array_key_exists($page, $pages))
{
	http_response_code(404);
	die();
}

?>

<html>
	<!doctype html>
	<html lang="en">
	<head>
		<meta charset="utf-8" />
		<link rel="icon" type="image/png" href="img/logo.png">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
		<title>OwnSound</title>
	
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<meta name="viewport" content="width=device-width" />
	
		<!-- Bootstrap core CSS     -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">	
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/highlight.js/latest/styles/github.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.9.0/css/bootstrap-slider.css" />
	
		<!--     Fonts and icons     -->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
	
		<!--		Extra CSS			 -->
		<link href="css/style.css" rel="stylesheet" />
	
	</head>
<body>

<?php 

	// Include all the modals
	foreach (glob("pages/modals/*.html") as $filename)
	{
		include $filename;
	}
	
	// Include the main page
	include($pages[$page]); 
?>

</body>
	<!--   Core JS Files	-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>

	<!--	Dependencies	-->
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<script src="https://cdn.jsdelivr.net/highlight.js/latest/highlight.min.js"></script>

	<!--	Bootstrap		-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.9.0/bootstrap-slider.min.js"></script>

	<!--	Scripts			-->
	<script src="js/loading_modal.js"></script>

	<?php
	if ($page != "login")
	{
	?>
	<script src="js/extensions.js"></script>
	<script src="js/main.js"></script>
	<script src="js/lyrics.js"></script>
	<script src="js/forms.js"></script>
	<script src="js/player.js"></script>
	<script src="js/app.js"></script>
	<script src="js/youtube.js"></script>
	<script src="js/keyboard.js"></script>

	<?php
	}
	?>

</html>