
function parseHtml(page)
{
	page = page.replace(/src="[^"]*"/ig, "")
		.replace(/href="[^"]*"/ig, "")
		.replace(/<\s*(script|iframe)[^>]*>(?:[^<]*<)*?\/\1>/g, "")
		.replace(/(<(\b(img|style|head|link)\b)(([^>]*\/>)|([^\7]*(<\/\2[^>]*>)))|(<\bimg\b)[^>]*>|(\b(background|style)\b=\s*"[^"]*"))/g, "");

	return $(page);
}

function storeLyrics(song, lyrics)
{
	$.ajax({
		url: 'api/store_lyrics.php',
		method: 'POST',
		data: {
			song: song.mbid,
			lyrics: lyrics
		},
		success: function(resp)
		{
			console.log('Lyrics stored for song: ' + song.mbid)
		}
	});
}

function metrolyrics(song)
{
	$.get({
		url: 'api/lyrics/metrolyrics.php',
		data: {
			artist: song.artist.name,
			title: song.title
		},
		success: function(resp, satus, xhr)
		{
			$('#lyrics').empty();

			var respPage = parseHtml(resp);

			var found = false;

			if (xhr.status == 200)
			{
				respPage.find('.verse').each(function(i, obj) 
				{ 
					$('#lyrics').append(
						$("<div class='row lyrics-verse'>")
							.append(
								$("<div class='col-md-12'>")
									.append(
										$(obj).html()
									).append(
										"<br><br>"
									)
							)
					); 
					found = true;
				});
			}

			if (found)
			{
				storeLyrics(song, $('#lyrics').html());
			}
			else
			{
				azlyrics(song);
			}
		},
		error: function(err)
		{
			console.log(err);
		}
	})
}

function genius(song)
{
	$.get({
		url: 'api/lyrics/genius.php',
		data: {
			artist: song.artist.name,
			title: song.title
		},
		success: function(resp, satus, xhr)
		{
			$('#lyrics').empty();

			var respPage = parseHtml(resp);

			var respDivs = respPage.find('.lyrics p');
			
			$('#lyrics').append(
				respDivs[0]
			);

			if (respDivs.length > 0)
			{
				storeLyrics(song, $('#lyrics').html());
			}
			else
			{
				metrolyrics(song);
			}
		},
		error: function(err)
		{
			console.log(err);
		}
	})
}

function songlyrics(song)
{
	$.get({
		url: 'api/lyrics/songlyrics.php',
		data: {
			artist: song.artist.name,
			title: song.title
		},
		success: function(resp, satus, xhr)
		{
			$('#lyrics').empty();

			var respPage = parseHtml(resp);

			var respDivs = respPage.find('#songLyricsDiv');
			
			$('#lyrics').append(
				respDivs[0]
			);

			if (respDivs.length > 0)
			{
				storeLyrics(song, $('#lyrics').html());
			}
			else
			{
				notFound();
			}
		},
		error: function(err)
		{
			console.log(err);
		}
	})
}


function azlyrics(song)
{
	$.get({
		url: 'api/lyrics/azlyrics.php',
		data: {
			artist: song.artist.name,
			title: song.title
		},
		success: function(resp, satus, xhr)
		{
			$('#lyrics').empty();

			var respPage = parseHtml(resp);

			var respDivs = respPage.find('.col-xs-12.col-lg-8.text-center').find('div');
			
			$('#lyrics').append(
				respDivs[6]
			);

			$('#lyrics').html($('#lyrics').html().split('-->')[1]);

			if (respDivs.length > 0)
			{
				storeLyrics(song, $('#lyrics').html());
			}
			else
			{
				songlyrics(song);
			}
		},
		error: function(err)
		{
			console.log(err);
		}
	})
}

function notFound()
{
	$('#lyrics').append('No lyrics found for this song.');	
}

function existing(song)
{
	getLyrics(song.mbid, function(lyrics) 
	{
		if (lyrics != null)
		{
			$('#lyrics').html(lyrics);
		}
		else
		{
			// Lyrics not found
			genius(song);
		}
	});
}

function getLyrics(mbid, callback)
{
	$.get({
		url: 'api/lyrics.php',
		data: {
			song: mbid
		},
		success: function(resp, status, xhr)
		{
			callback(resp);
		},
		error: function(xhr, msg, error)
		{
			callback(null);
		}
	});
}

function refreshLyrics()
{
	var song = audioPlayer.currentSong();

	if (song != null)
	{
		$('#lyrics').empty().append(
			$("<div class='row'>")
				.append(
					$("<div class='col-md-2 col-md-offset-5'>")
						.append($("<img src='img/ajax-loader.gif'>"))
				)
		);
		
		// Call in order:	genius -> metrolyrics -> azlyrics
		existing(song);
	}
}
