
var typing = false;

function onKeyPress(key)
{
	//console.log(key);
	switch (key)
	{
		/*
			Song control
		*/
		case 37:	//Left
			audioPlayer.previous();
			return true;
			
		case 39:	// Right
			audioPlayer.next();
			return true;
			
		case 32:	// Spacebar
			audioPlayer.toggle();
			return true;
			
		case 83:	// S
			shuffle();
			return true;

		/*
			Volume
		*/
		case 38:	// Up
		case 40:	// Down
			return onKeyHold(key);
	}
	return false;
}

function onKeyHold(key)
{
	switch (key)
	{
		/*
			Volume
		*/
		case 38:	// Up
			audioPlayer.changeVolume(0.05);
			return true;

		case 40:	// Down
			audioPlayer.changeVolume(-0.05);
			return true;
	}
	return false;
}

function refreshKeyboardInputBinds()
{
	$('input[type=text], input[type=email], input[type=password]')
	.off('focusin').focusin(function()
	{
		typing = true;		
	})
	.off('focusout').focusout(function()
	{
		typing = false;
	});
}

$(document).ready(function() 
{
	var keysPressed = {};


	$(document).keydown(function(e)
	{
		if (typing)
			return;

		if (keysPressed[e.keyCode])
		{
			if (onKeyHold(e.keyCode))
				e.preventDefault();
			
			return;
		}
		keysPressed[e.keyCode] = true;
		
		if (onKeyPress(e.keyCode))
		{
			e.preventDefault();
		}
	});
	$(document).keyup(function(e)
	{
		delete keysPressed[e.keyCode];
	});

	
	refreshKeyboardInputBinds();
});