$(document).ready(function() 
{
	/*
	callbacks = {
		onSubmit: function() {
			Called when the form is submitted
		},
		onResponse: function(response, status, xhr)
		{
			Called when the response is received
		},
		onError: function(xhr, status, error)
		{
			Called on error
		},
		onProgress: function(progressPercent)
		{
			Called on upload progress
		},
		onDownloadProgress: function(progressPercent)
		{
			Called on download progress
		}
	}
	*/
	$.fn.ajaxForm = function(callbacks) 
	{
		this.unbind().submit(
			function(event)
			{
				var thisForm = $(this);
				var btnContainer = $(this).find('.ajax-btn-container');
				var loadingContainer = $(this).find('.ajax-loading');

				event.preventDefault();

				btnContainer.hide();
				loadingContainer.show();

				if (callbacks.onSubmit && callbacks.onSubmit(thisForm))
				{
					return;
				}

				thisForm.ajaxSubmit({
					type: 'POST',
					success: function(resp, status, xhr)
					{
						loadingContainer.hide();
						btnContainer.show();

						if (callbacks.onResponse)
						{
							callbacks.onResponse(resp, status, xhr);
						}
					},
					error: function(xhr, status, error)
					{
						if (callbacks.onError)
						{
							callbacks.onError(xhr, status, error);
						}
					},
					xhr: function()
					{
						var xhr = new window.XMLHttpRequest();
						xhr.upload.addEventListener('progress', function(evt)
						{
							if (evt.lengthComputable)
							{
								var percent = evt.loaded / evt.total;

								if (callbacks.onProgress)
								{
									callbacks.onProgress(percent);
								}
							}
						}, false);
						xhr.addEventListener('progress', function(evt)
						{
							if (evt.lengthComputable)
							{
								var percent = evt.loaded / evt.total;

								if (callbacks.onDownloadProgress)
								{
									callbacks.onDownloadProgress(percent);
								}
							}
						}, false);
						
						return xhr;
					}
				});
			}
		);
		return this;
	};

	$.fn.ajaxBtn = function()
	{
		this.change(
			function(e) {
				$(this).closest('form').submit();
			}
		);
	}
});