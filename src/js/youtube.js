

function timestamp()
{
	return Math.round((new Date()).getTime() / 1000);
}

var startedConversionTime;

function updateProgress(mbid)
{
	$.get({
		url: 'api/youtube/progress.php',
		data: {
			mbid: mbid
		},
		success: function(resp)
		{
			if (resp.status == "done")
			{
				onComplete();
				return;
			}

			var val = Math.round(resp.progress * 100);
			$('#progress-modal').find('.progress-bar').css({ width: val + '%' });
			$('#progress-modal').find('.progress-value').html(val + '%');

			var duration = timestamp() - startedConversionTime;
			var timeLeft = Math.round(duration / resp.progress) - duration;

			$('#progress-modal').find('.time-left').html('Time left: ' + timeFormat(timeLeft));

			setTimeout(function()
			{
				updateProgress(mbid);
			}, 2000);
		}
	});
}

function onComplete()
{
	$('#progress-modal').modal('hide');

	refreshSongList();
	refreshArtistList();
}

$(document).ready(function() 
{

	$('#youtube-form').ajaxForm({

		onSubmit: function()
		{
			$('#upload-modal').modal('hide');
			$('#progress-modal').find('.progress-description').empty();
			$('#progress-modal').find('.modal-title').html('Downloading...');
			$('#progress-modal').find('.progress-bar').css({ width: '0%' });
			$('#progress-modal').find('.progress-value').html('0%');
			$('#progress-modal').modal();
		},

		onResponse: function(resp)
		{
			console.log(resp);

			// Server has downloaded the video and is now converting it to mp3
			$('#progress-modal').find('.modal-title').html('Converting...');
			$('#progress-modal').find('.progress-description').append(
				$("<div class='col-md-9'>").append(resp.youtube_info.title)
			).append(
				$("<div class='col-md-3 time-left'>")
			);

			startedConversionTime = timestamp();

			var trackMbid = resp.info.track.mbid;
			updateProgress(trackMbid);
		}

	});
});