

var filters = {
};
var filterLabels = {};
var sort = "date";

var playlist = [];

function setSort(sortBy)
{
	sort = sortBy;
	refreshSongList();
}

function addFilter(key, val, label)
{
	if (key == 'artist' && filters['album'])
	{
		delete filters['album'];
		delete filterLabels['album'];
	}
		
	if (key == 'album' && filters['artist'])
	{
		delete filters['artist'];
		delete filterLabels['artist'];
	}

	filters[key] = val;
	filterLabels[key] = label;
	refreshSongList();
	updateFilterLabels();
}

function removeFilter(key)
{
	delete filters[key];
	delete filterLabels[key];
	refreshSongList();
	updateFilterLabels();
}

function shuffle()
{
	sort = "random";
	refreshSongList(function()
	{
		audioPlayer.playSong(playlist[0], playlist);
	});
}

function updateFilterLabels()
{
	$('#filter-labels').empty();

	for (var key in filterLabels)
	{
		var label = filterLabels[key];
		$('#filter-labels').append(
			$("<span class='label label-primary filter-label'>")
				.append(label).append('&nbsp;')
				.append(
					$("<i class='fa fa-times clickable' onclick='removeFilter(\"" + key + "\")'>")
				)
				.css({
					'margin-right': '1px'
				})
		);
	}
}

function editSong(mbid)
{
	$.get({
		url: 'api/songs.php',
		data: { song: mbid },
		success: function(resp, status, xhr)
		{
			var song = resp[0];

			$('#edit-song_mbid').attr('value', song.mbid);
			$('#edit-song_title').attr('value', song.title);
			$('#edit-song_artist').val(song.artist.mbid).change();

			// Load the lyrics
			getLyrics(song.mbid, function(lyrics)
			{
				$('#edit-lyrics').html(lyrics);
			});

			

			$('#edit-modal').modal();
		}
	});
}

function editProfile()
{
	$.get({
		url: 'api/users.php',
		data: { me: 1 },
		success: function(resp, status, xhr)
		{
			var user = resp[0];

			$('#edit-user_id').attr('value', user.user_id);
			$('#edit-username').attr('value', user.username);
			$('#edit-email').attr('value', user.email);

			
			$('#profile-modal').modal();
		}
	});
}

function createUser()
{
	$('#modal-edit-body').empty().html(registrationForm());
	refreshKeyboardInputBinds();
	
	$('#edit-modal').find('.modal-title').html('Create User');
	$('#edit-modal').modal();
}

function songListItem(song)
{
	var thumbnail = song.album.img.small != '' ? song.album.img.small : 'img/placeholder.png';

	var row = $("<div class='row song-row'>")
		.attr('id', song.mbid)
		.append(
			$("<div class='col-xs-2'>")
				.append(
					$("<div class='thumbnail-container'>")
						.append(
							$("<img class='img-responsive'>").attr('src', thumbnail)
						)
						.append(
							$("<div class='overlay thumbnail-overlay play'>")
								.append("<i class='fa fa-play fa-lg'>")
						).append(
							$("<div class='overlay thumbnail-overlay pause'>")
								.append("<i class='fa fa-pause fa-lg'>")
						)
				)
		).append(
			$("<div class='col-xs-8'>")
				.append(
					song.title
				)
				.append("<br>")
				.append(
					$("<small class='clickable-text'>")
						.append(song.artist.name)
						.click(function(e)
						{
							e.preventDefault();
							e.stopPropagation();
							addFilter('artist', song.artist.mbid, song.artist.name);
							return false;
						})
				)
		).append(
			$("<div class='col-xs-offset-1 col-xs-1'>")
				.append(
					$("<span class='song-duration'>")
						.append(formatDuration(song.duration))
				).append(
					$("<br><br>&nbsp;")
				)
				.append(
					$("<i class='fa fa-pencil fa-lg clickable song-edit'>")
						.click(function(e)
						{
							e.preventDefault();
							e.stopPropagation();
							editSong(song.mbid);
							return false;
						})
				)
		).click(function()
		{
			audioPlayer.playSong(song.mbid, playlist);
		});

	if (audioPlayer.currentSong() != null && audioPlayer.currentSong().mbid == song.mbid)
	{
		row.addClass('selected');
	}

	return row;
}

function artistListItem(artist)
{
	var artistThumbnail = artist.img.medium != '' ? artist.img.medium : 'img/placeholder.png';
	return $("<div class='col-xs-4 artist-item overlay-container' data-toggle='tooltip' data-placement='auto'>")
		.append(
			$("<img class='artist-thumbnail img-responsive'>").attr('src', artistThumbnail)
		).append(
			$("<div class='overlay artist-overlay'>")
				.append("<i class='fa fa-search fa-2x'>")
		)
		.attr('title', artist.name)
		.click(function()
		{
			addFilter('artist', artist.mbid, artist.name);
		});
}

function albumListRow(artist)
{
	var artistThumbnail = artist.img.medium != '' ? artist.img.medium : 'img/placeholder.png';
	var row = $("<div class='row'>")
		.append(
			$("<div class='col-md-3 col-xs-3 artist-item overlay-container'>")
				.append(
					$("<img class='artist-thumbnail img-responsive'>").attr('src', artistThumbnail)
				).append(
					$("<div class='overlay'>")
						.append("<i class='fa fa-search fa-2x'>")
				).click( (function()
				{
					var mbid = artist.mbid;
					var name = artist.name;
					return function()
					{
						addFilter('artist', mbid, name);
					}
				})() )
		).append(
			$("<div class='col-md-9 col-xs-9'>")
				.append(
					$("<h4>").append(artist.name)
				)
				.append(
					$("<span class='artist-album-list'>")
				)
		)
	
	for (var i = 0; i< artist.albums.length; i++)
	{
		var album = artist.albums[i];

		if (album.song_count == 0)
			continue;

		var albumThumbnail = album.img.small != '' ? album.img.small : 'img/placeholder.png';
		row.find('.artist-album-list')
			.append(
				$("<span class='clickable'>")
					.append(
						$("<img class='album-thumbnail' data-toggle='tooltip' data-placement='auto'>")
							.attr('src', albumThumbnail)
							.attr('title', album.name)
					).click( (function()
					{
						var mbid = album.mbid;
						var name = album.name;
						return function()
						{
							addFilter('album', mbid, name);
						}
					})() )
			);
	}

	return row;
}

function artistSelectOption(artist)
{
	return $("<option>")
		.attr('value', artist.mbid)
		.html(artist.name);
}

function albumSelectOption(album)
{
	return $("<option>")
		.attr('value', album.mbid)
		.html(album.name);
}

function userListItem(user)
{
	return $("<div class='row user-item'>")
		.append(
			$("<div class='col-md-3 col-xs-3'>")
				.append(
					$("<img class='img-fluid user-thumbnail clickable'>")
						.attr('src', "https://secure.gravatar.com/avatar/" + user.gravatar_hash + "?s=48&d=robohash")
						.click(function(e)
						{
							e.preventDefault();
							e.stopPropagation();
							addFilter('user', user.user_id, user.username);
							return false;
						})
				)
		).append(
			$("<div class='col-md-9 col-xs-9'>")
				.append(
					$("<div class='row'>")
						.append(
							$("<div class='col-md-12'>")
								.append(
									$("<i class='fa fa-circle online-status'>")
										.css({ color:  user.online ? "green" : "grey" })
										.attr('title', user.online ? "Online" : "Offline")
								).append('&nbsp;')
								.append(
									$("<strong class='clickable-text'>")
										.append(user.username)
										.click(function(e)
										{
											e.preventDefault();
											e.stopPropagation();
											addFilter('user', user.user_id, user.username);
											return false;
										})
								)
							)
				).append(
					$("<div class='row'>")
						.append(
							$("<div class='col-md-12 user-listening'>")
								.append(
									user.online ?
										$("<span class='overflow-text'>")
											.append(
												user.listening != undefined ? 
												$("<i class='clickable-text'>")
													.append(user.listening.artist.name + " - " + user.listening.title)
													.click(function(e)
													{
														e.preventDefault();
														e.stopPropagation();
														audioPlayer.playSong(user.listening.mbid);
														return false;
													})
													: ""
											)
										: "Last online: " + secondsToTimeAgo(user.last_seen)
								)
						)
				)
		);
}

function refreshSongList(callback = null)
{
	var params = Object.assign({}, filters);
	params['sort'] = sort;
	
	$.ajax({
		url: 		'api/songs.php',
		method:		'GET',
		data:		params,
		success:	function(resp, status, xhr)
		{
			$('#song-list').empty();
			playlist = [];

			for (var i = 0; i < resp.length; i++)
			{
				$('#song-list').append(
					songListItem(resp[i])
				);
				playlist.push(resp[i].mbid);
			}
				
			
			if (callback != null)
			{
				callback();
			}
		},
		error:		function(xhr, msg, err)
		{
			console.log(xhr);
		}
	});
}

function refreshArtistList()
{
	$.ajax({
		url: 		'api/artists.php',
		method:		'GET',
		contentType: "application/json; charset=utf-8",
		success:	function(resp, status, xhr)
		{
			$('#artist-list').empty();
			$('#album-list').empty();
			$('#edit-song_artist').empty(); // Let's also update the editing form while we have the artists
			
			for (var i = 0; i < resp.length; i++)
			{
				var songCount = 0;
				for (var j = 0; j < resp[i].albums.length; j++)
					songCount += resp[i].albums[j].song_count;

				if (songCount == 0)
					continue;

				$('#artist-list').append(
					artistListItem(resp[i])
				);
				$('#album-list').append(
					albumListRow(resp[i])
				);
				if (resp[i].name)
				{
					$('#edit-song_artist').append(
						artistSelectOption(resp[i])
					);
				}
			}
			$('[data-toggle="tooltip"]').tooltip();
		},
		error:	function(xhr, msg, err)
		{
			console.log(err);
		}
	});
}

function refreshUserList()
{
	$.ajax({
		url: 		'api/users.php',
		method:		'GET',
		success:	function(resp, status, xhr)
		{
			$('#user-list').empty();
			
			for (var i = 0; i < resp.length; i++)
			{
				$('#user-list').append(
					userListItem(resp[i])
				);
			}
		}
	});
}

$(document).ready(function() 
{
	refreshSongList(function()
	{
		if (playlist.length > 0 && audioPlayer.currentSong() == null)
		{
			audioPlayer.setSong(playlist[0], playlist);
		}
	});

	refreshArtistList();


	setInterval(refreshUserList, 1000);
});