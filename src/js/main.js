

function str_pad_left(string,pad,length) {
    return (new Array(length+1).join(pad)+string).slice(-length);
}

function timeFormat(time)
{
	time = Math.round(time);
	var minutes = Math.floor(time / 60);
	var seconds = time - minutes * 60;
	return str_pad_left(minutes,'0',2)+':'+str_pad_left(seconds,'0',2);
}

function formatDuration(duration)
{
	var formatted = duration % 60;
	if (formatted < 10) formatted = '0' + formatted;
	if (duration > 60) formatted  = Math.floor(duration / 60) + ':' + formatted;

	return formatted;
};

function secondsToTimeAgo(sec)
{
	if (sec < 0)
	{
		return "Never";
	}
	if (sec < 60)
	{
		return "Just now";
	}
	if (sec < 3600)
	{
		return Math.floor(sec / 60) + " minutes ago";
	}
	if (sec < 86400)
	{
		return Math.floor(sec / 3600) + " hours ago";
	}
	if (sec < 2592000)
	{
		return Math.floor(sec / 86400) + " days ago";
	}
	return Math.floor(sec / 2592000) + " months ago";
}

$(document).ready(function() 
{
	// Register all tooltips
	$('[data-toggle="tooltip"]').tooltip();

	// Register all ajax buttons
	$('.ajax-btn').ajaxBtn();

	// Upload button
	$('#upload-form').ajaxForm({
		onSubmit: function()
		{
			$('#upload-modal').modal('hide');
			$('#progress-modal').find('.progress-description').empty();
			$('#progress-modal').find('.modal-title').html('Uploading...');
			$('#progress-modal').find('.progress-bar').css({ width: '0%' });
			$('#progress-modal').find('.progress-value').html('0%');
			$('#progress-modal').modal();
		},
		onResponse: function(resp, status, xhr)
		{
			console.log(resp);
			$('#progress-modal').modal('hide');

			refreshSongList();
			refreshArtistList();
		},
		onError: function(xhr, status, error)
		{
			$('#progress-modal').modal('hide');
			$('#upload-modal').modal('hide');
			console.log('Error: ' + error);
			console.log(xhr.responseText);
		},
		onProgress: function(percent)
		{
			var val = Math.round(percent * 100);
			$('#progress-modal').find('.progress-bar').css({ width: val + '%' });
			$('#progress-modal').find('.progress-value').html(val + '%');
		}
	});

	// Search form
	$('#search-form').ajaxForm({
		onSubmit: function()
		{
			var val = $('#searchbar').val();
			addFilter('search', val, val);
			$('#searchbar').val('');
			return true;
		},
		onResponse: function(resp)
		{
			console.log(resp);
		},
		onError: function(xhr, status, error)
		{
			console.log(error);
		}
	});

	// Edit profile form
	$('#profile-form').ajaxForm({
		onResponse: function(resp, status, xhr)
		{
			if (resp == "OK")
			{
				$('#profile-modal').modal('hide');
			}
			else
			{
				alert(resp);
			}
		}
	});

	// Add song button
	$('#add-song-btn').click(function()
	{
		$('#upload-modal').modal();
	});

	// Song editing
	// Make the artist select value changing also update the album
	$('#edit-song_artist').change(function()
	{
		var artistMbid = $('#edit-song_artist').val();

		// Load the artist's albums
		$('#edit-song_album').empty();
		$.get({
			url: 'api/artists.php',
			data: { artist: artistMbid },
			success: function(resp, status, xhr)
			{
				var artist = resp[0];

				for (var i = 0; i < artist.albums.length; i++)
				{
					var album = artist.albums[i];

					$('#edit-song_album').append(
						albumSelectOption(album)
					);

				}

				$('#edit-song_album').val(song.album.mbid);
			}
		});
	});

	$('.audio-slider').slider().data('slider');
});