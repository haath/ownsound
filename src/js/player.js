


var audioPlayer = audioPlayer || (function($) 
{
	var player = () => document.getElementById('audio-player');
	var playBtn = () => $('#audio-toggle-btn');

	var currentSong = null;
	var repeat = false;

	var sliderLock = false;

	var playlist = [];

	return {

		init: function()
		{

		},

		toggleRepeat: function()
		{
			repeat = !repeat;
			console.log(repeat);
			if (repeat)
			{
				$('#repeat-btn').addClass('control-selected');
			}
			else
			{
				$('#repeat-btn').removeClass('control-selected');
			}
		},

		currentSong: function()
		{
			return currentSong;
		},

		songDuration: function()
		{
			if (currentSong !=null && currentSong.duration > 0)
				return currentSong.duration;
			else
				return player().duration;
		},

		updatePlayer: function(trackId)
		{
			$.ajax({
				url: 'api/songs.php',
				method: 'GET',
				data: { song: trackId },
				success: function(resp, status, xhr)
				{
					audioPlayer.onBufferUpdate();

					currentSong = resp[0];
					currentSong.timestamp = Date.now();

					var artwork = currentSong.album.img.large != '' ? currentSong.album.img.large : 'img/placeholder.png';
					// Album artwork
					$('#playing-artwork').attr('src', artwork)
										.attr('title', currentSong.album.name);

					// Artist name
					$('#playing-artist')
						.html(currentSong.artist.name)
						.off('click')
						.click(function()
						{
							addFilter('artist', currentSong.artist.mbid, currentSong.artist.name)	
						});
					$('#playing-song').html(currentSong.title);

					$('#playback-duration').html(timeFormat(audioPlayer.songDuration()));

					if ($('#lyrics').hasClass('active'))
					{
						refreshLyrics();
					}

					if (player().duration != undefined && player().duration > 0 && currentSong.duration != Math.round(player().duration))
					{
						var duration = Math.round(player().duration);
						console.log('Correcting duration: ' + currentSong.mbid + ' -> ' + duration);
						$.ajax({
							url: 'api/edit_song.php',
							method: 'POST',
							data: {
								'song-mbid': currentSong.mbid,
								title: currentSong.title,
								duration: duration
							},
							success: function(resp)
							{
								$('#' + trackId).find('.song-duration').html(formatDuration(duration));
							}
						});
					}
				}
			});
		},

		play: function()
		{
			player().play();
		},
		
		pause: function()
		{
			player().pause();
		},

		previous: function()
		{
			var curIndex = playlist.indexOf(currentSong.mbid);
			var previousIndex = Math.max(curIndex - 1, 0);
			audioPlayer.changeSong(playlist[previousIndex], playlist);
		},
		next: function()
		{
			var curIndex = playlist.indexOf(currentSong.mbid);
			var nextIndex = curIndex + 1 < playlist.length ? curIndex + 1 : 0;
			audioPlayer.changeSong(playlist[nextIndex], playlist);
		},

		/*
			If the current song is playing then it sets the new song also starts playing it.
			Otherwise it only sets the new song.
		*/
		changeSong: function(trackId, list = [])
		{
			if (audioPlayer.isPlaying())
				audioPlayer.playSong(trackId, list);
			else
				audioPlayer.setSong(trackId, list);
		},

		toggle: function()
		{
			if (audioPlayer.isPlaying())
			{
				audioPlayer.pause();
			}
			else
			{
				audioPlayer.play();
			}
		},

		setSong: function(trackId, list = [])
		{
			if (currentSong == null || trackId != currentSong.mbid)
			{
				player().setAttribute('preload', 'auto');
				player().setAttribute('src', 'https://music.gmantaos.com/api/music.php?song=' + trackId);

				audioPlayer.updatePlayer(trackId);
				audioPlayer.onBufferUpdate();

				audioPlayer.updateUser();

				
				$('.song-row').removeClass('selected');
				$('#' + trackId).addClass('selected');	
			}
			playlist = list;
		},

		playSong: function(trackId, list = [])
		{
			if (currentSong == null || trackId != currentSong.mbid)
			{
				player().pause();

				audioPlayer.setSong(trackId, list);
				player().play();


			}
			else if (player().paused)
			{
				player().play();
			}
			else
			{
				player().pause();
			}
		},

		updateUser: function()
		{
			var data = {};
			if (currentSong != null && audioPlayer.isPlaying())
			{
				data.song = currentSong.mbid;
			}

			$.ajax({
				url: 'api/user_update.php',
				method: 'POST',
				data: data,
				success: function(resp) {}
			});
		},

		isPlaying: function()
		{
			return !player().paused;
		},

		onTimeUpdate: function(ev)
		{
			$('#playback-time').html(timeFormat(player().currentTime));

			if (!sliderLock)
			{
				var sliderVal = currentSong == null ? 0 : Math.round(10000 * player().currentTime / audioPlayer.songDuration());
				$('#song-slider').slider('setValue', sliderVal);
			}
		},

		onBufferUpdate: function(ev)
		{
			var bufferedRanges = player().buffered;
			var highlightEnd = 0;

			for (var i = 0; i < bufferedRanges.length; i++)
			{
				var end = Math.round(10000 * bufferedRanges.end(i) / audioPlayer.songDuration());
				
				if (end > highlightEnd)
					highlightEnd = end;
			}

			$('#song-slider').slider({
				rangeHighlights: [{
					start: 0,
					end: highlightEnd,
					class: 'buffered'
				}]
			});
		},

		onPlaybackStart: function(ev)
		{
			playBtn().removeClass('fa-play-circle').addClass('fa-pause-circle');
			$(document).attr('title', currentSong.artist.name + ' - ' + currentSong.title);
		},
		onPlaybackStop: function(ev)
		{
			playBtn().removeClass('fa-pause-circle').addClass('fa-play-circle');
			$(document).attr('title', 'OwnSound');
		},
		onPlaybackEnd: function(ev)
		{
			if (repeat)
			{
				audioPlayer.play();
			}
			else
			{
				audioPlayer.next();
				audioPlayer.play();
			}
		},

		onError: function(ev)
		{
			console.log(player().error);
		},

		changeVolume: function(val)
		{
			player().volume = Math.min(Math.max(player().volume + val, 0), 1);
			var sliderVal = Math.round(player().volume * 100);
			$('#volume-slider').slider('setValue', sliderVal);
		},

		onVolumeChanged: function(ev)
		{
			player().volume = ev.value / 100;
		},

		onSlideStart: function(ev)
		{
			sliderLock = true;
		},
		onSlideStop: function(ev)
		{
			if (currentSong != null)
			{
				var songTime = audioPlayer.songDuration() * (ev.value / 10000);
				player().currentTime = songTime;
			}

			sliderLock = false;
		}
	}
})(jQuery);


$(document).ready(function() 
{
	audioPlayer.init();

	// Set event handlers for audio player
	$('#audio-player').on('timeupdate', audioPlayer.onTimeUpdate)
					.on('progress', audioPlayer.onBufferUpdate)
					.on('playing', audioPlayer.onPlaybackStart)
					.on('pause', audioPlayer.onPlaybackStop)
					.on('ended', audioPlayer.onPlaybackEnd)
					.on('error', audioPlayer.onError);
	
	// Initialize song slider with event handlers and time formatter				
	$('#song-slider').slider({
		formatter: function(val)
		{
			return timeFormat(Math.round(audioPlayer.songDuration() * val / 10000));
		}
	}).on('slideStop', audioPlayer.onSlideStop)
	.on('slideStart', audioPlayer.onSlideStart);

	// Set volume control events
	$('#volume-slider').slider().on('slide', audioPlayer.onVolumeChanged)
								.on('slideStop', audioPlayer.onVolumeChanged);

	// Set user status update interval
	setInterval(audioPlayer.updateUser, 5000);
});