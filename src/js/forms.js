function songEditForm(song)
{
	return $("<form method='POST' action='api/edit_song.php'>")
		.append(
			$("<input type='hidden' name='song-mbid'>").attr('value', song.mbid)
		).append(
			$("<input type='hidden' name='album-mbid'>").attr('value', song.album.mbid)
		).append(
			$("<input type='hidden' name='artist-mbid'>").attr('value', song.artist.mbid)
		).append(
			$("<input type='hidden' id='delete-song' name='delete'>").attr('value', '0')
		).append(
			$("<div class='row'>")
				.append(
					$("<div class='col-md-12'>")
						.append(
							$("<div class='form-group'>")
								.append(
									$("<label for='song-title'>").append("Title")
								).append(
									$("<input type='text' name='title' id='song-title' class='form-control'>").attr('value', song.title)
								)
						)
				)
		).append(
			$("<div class='row'>")
				.append(
					$("<div class='col-md-12'>")
						.append(
							$("<div class='form-group'>")
								.append(
									$("<label for='song-duration'>").append("Duration")
								).append(
									$("<input type='number' name='duration' id='song-duration' class='form-control'>").attr('value', song.duration)
								)
						)
				)
		).append(
			$("<div class='row'>")
				.append(
					$("<div class='col-md-12'>")
						.append(
							$("<div class='form-group'>")
								.append(
									$("<label for='song-artist'>").append("Artist")
								).append(
									$("<input type='text' name='artist' id='song-artist' class='form-control'>").attr('value', song.artist.name)
								)
						)
				)
		).append(
			$("<div class='row'>")
				.append(
					$("<div class='col-md-12'>")
						.append(
							$("<div class='form-group'>")
								.append(
									$("<label for='song-album'>").append("Album")
								).append(
									$("<input type='text' name='album' id='song-album' class='form-control'>").attr('value', song.album.name)
								)
						)
				)
		).append(
			$("<div class='row'>")
				.append(
					$("<div class='col-md-12'>")
						.append(
							$("<div class='form-group'>")
								.append(
									$("<label for='song-year'>").append("Year")
								).append(
									$("<input type='text' name='year' id='song-year' class='form-control'>").attr('value', song.album.year)
								)
						)
				)
		).append(
			$("<div class='row'>")
				.append(
					$("<div class='col-md-4'>")
						.append(
							$("<input type='submit' class='btn btn-primary' name='submit' value='Save'>")
						)
				)
				.append(
					$("<div class='col-md-offset-3 col-md-4'>")
						.append(
							$("<button class='btn btn-danger'>")
								.append("Delete")
								.click(function(e)
								{
									$('#delete-song').attr('value', '1')
								})
						)
				)
		).ajaxForm({
			onResponse: function(resp, status, xhr)
			{
				console.log(resp);
				$('#edit-modal').modal('hide');
				refreshArtistList();
			},
			onError: function(xhr, status, error)
			{
				console.log(error);
			}
		});
}

function registrationForm()
{
	return $("<form method='POST' action='api/create_user.php'>")
		.append(
			$("<div class='row'>")
				.append(
					$("<div class='col-md-12'>")
						.append(
							$("<div class='form-group'>")
								.append(
									$("<input type='text' name='username' id='username' class='form-control' placeholder='Username' required>")
								)
						)
				)
		).append(
			$("<div class='row'>")
				.append(
					$("<div class='col-md-12'>")
						.append(
							$("<div class='form-group'>")
								.append(
									$("<input type='text' name='email' id='email' class='form-control' placeholder='E-mail (optional)' >")
								)
						)
				)
		).append(
			$("<div class='row'>")
				.append(
					$("<div class='col-md-12'>")
						.append(
							$("<div class='form-group'>")
							.append(
									$("<input type='password' name='password' id='password' placeholder='Password' class='form-control' required>")
								)
						)
				)
		).append(
			$("<div class='row'>")
			.append(
				$("<div class='col-md-12'>")
					.append(
						$("<div class='form-group'>")
							.append(
								$("<input type='password' name='password2' id='password2' placeholder='Password (re-type)' class='form-control' required>")
							)
					)
			)
		).append(
			$("<div class='row'>")
				.append(
					$("<div class='col-md-12'>")
						.append(
							$("<div class='form-group'>")
								.append(
									$("<input type='number' name='quota' id='quota' class='form-control' placeholder='Quota(MB) (optional)'>")
								)
						)
				)
		).append(
			$("<div class='row'>")
				.append(
					$("<div class='col-md-4'>")
						.append(
							$("<input type='submit' class='btn btn-primary' value='Create'>")
						)
				)
		).ajaxForm({
			onResponse: function(resp, status, xhr)
			{
				if (resp == "OK")
				{
					$('#edit-modal').modal('hide');
					refreshUserList();
				}
				else
				{
					alert(resp);
				}
			}
		});
}