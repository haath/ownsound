# OwnSound

Self-hosted private music server.

![Screenshot](assets/screenshot.png)

- Uploading mp3 files
- Fetching artwork and metadata from [Last.fm](https://www.last.fm/api/intro)
- Fetching lyrics from multiple websites
- Downloading and converting videos off of Youtube URLs
- Real-time display of what other users are listening to